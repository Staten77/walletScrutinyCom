---
wsId: bitpie
title: Bitpie-Universal Crypto Wallet
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2022-05-12
version: 5.0.104
stars: 3.7
reviews: 92
size: '328439808'
website: https://bitpie.com
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitpieWallet
social:
- https://www.facebook.com/BitpieOfficial
- https://www.reddit.com/r/BitpieWallet

---

 {% include copyFromAndroid.html %}
