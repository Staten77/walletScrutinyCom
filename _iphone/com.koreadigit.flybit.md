---
wsId: flybit
title: Flybit
altTitle: 
authors:
- danny
appId: com.koreadigit.flybit
appCountry: us
idd: 1570368673
released: 2021-07-07
updated: 2022-06-07
version: 1.11.6
stars: 0
reviews: 0
size: '71342080'
website: https://blog.naver.com/flybit
repository: 
issue: 
icon: com.koreadigit.flybit.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/flybit.exchange

---

{% include copyFromAndroid.html %}
