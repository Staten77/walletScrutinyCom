---
wsId: UpbitGlobal
title: Upbit Global
altTitle: 
authors:
- danny
appId: com.dunamu.upbit.global
appCountry: us
idd: 1439527412
released: 2018-11-06
updated: 2022-05-30
version: 1.7.6
stars: 3.3
reviews: 24
size: '82058240'
website: https://www.dunamu.com/
repository: 
issue: 
icon: com.dunamu.upbit.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/upbit-official
- https://www.facebook.com/upbit.exchange

---

{% include copyFromAndroid.html %}
