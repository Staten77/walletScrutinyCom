---
wsId: bitpaywallet
title: BitPay - Bitcoin Wallet & Card
altTitle: 
authors:
- leo
appId: com.bitpay.wallet
appCountry: 
idd: 1149581638
released: 2016-10-24
updated: 2022-05-19
version: 12.12.0
stars: 4
reviews: 1481
size: '88262656'
website: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2021-04-27
signer: 
reviewArchive: 
twitter: BitPay
social:
- https://www.linkedin.com/company/bitpay-inc-
- https://www.facebook.com/BitPayOfficial

---

{% include copyFromAndroid.html %}
