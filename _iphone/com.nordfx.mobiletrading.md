---
wsId: nordFX
title: NordFX
altTitle: 
authors:
- danny
appId: com.nordfx.mobiletrading
appCountry: lv
idd: 1551642767
released: 2021-02-06
updated: 2022-05-25
version: '15.0'
stars: 5
reviews: 1
size: '29656064'
website: https://nordfx.com/
repository: 
issue: 
icon: com.nordfx.mobiletrading.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-19
signer: 
reviewArchive: 
twitter: NordFX
social: 

---

{% include copyFromAndroid.html %}
