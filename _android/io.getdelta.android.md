---
wsId: getDelta
title: Delta Investment Tracker
altTitle: 
authors: 
users: 1000000
appId: io.getdelta.android
appCountry: us
released: 2017-09-23
updated: 2022-06-23
version: 2022.5.1
stars: 4.3
ratings: 26395
reviews: 1748
size: 
website: https://delta.app
repository: 
issue: 
icon: io.getdelta.android.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /io.getdelta.android/

---

This appears to be only a portfolio tracker. If it asks for your credentials for
exchanges, it might still get into a position of pulling your funds from there.
