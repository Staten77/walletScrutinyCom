---
wsId: 
title: Paymint - Secure Bitcoin Wallet
altTitle: 
authors:
- leo
users: 500
appId: com.paymintlabs.paymint
appCountry: 
released: 2020-06-29
updated: 2020-10-05
version: 1.2.2
stars: 
ratings: 
reviews: 
size: 
website: 
repository: https://github.com/Paymint-Labs/Paymint
issue: 
icon: com.paymintlabs.paymint.png
bugbounty: 
meta: stale
verdict: fewusers
date: 2021-10-01
signer: 
reviewArchive: 
twitter: paymint_wallet
social: 
redirect_from:
- /com.paymintlabs.paymint/

---

