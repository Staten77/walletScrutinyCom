---
wsId: 
title: HandCash
altTitle: 
authors: 
users: 50000
appId: io.handcash.wallet
appCountry: 
released: 2019-09-10
updated: 2022-06-19
version: 4.2.0
stars: 4.4
ratings: 581
reviews: 79
size: 
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive: 
twitter: handcashapp
social: 
redirect_from:
- /io.handcash.wallet/
- /posts/io.handcash.wallet/

---

A BSV wallet.
